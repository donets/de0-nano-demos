/**
 * Программа тестовая. По нажатию кнопки key0 на плате de0-nano, инкрементируется счетчик
 * counter, последние 8 разрядов которого отображаются на светодиодах платы.
 *
 * @author Donets Anton.
 */

module blink (
	input wire key0,
	output wire [7:0]led
);

reg [31:0]counter;

always @(posedge key0)
	counter <= counter + 1;			// По каждому нажатию инкрементировать счетчик counter
	
assign led = counter[7:0];			// Зажигать светодиоды в соответствии со значением в счетчике

endmodule
